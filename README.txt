MxVoid JWildfire Files README

These are the parameter files I use to make my IFS fractal flame renders with JWildfire,
a free, open-source, and cross-platform fractal explorer.

JWildfire is available at https://jwildfire.overwhale.com/

Most of these parameter files are licensed under a Creative Commons Attribution 4.0 International
License; please attribute to Robert Paul or MxVoid.

This means you are free to:
	> Share — copy and redistribute the material in any medium or format

	> Adapt — remix, transform, and build upon the material
	  for any purpose, even commercially.

Under the following terms:
	> Attribution — You must give appropriate credit, provide a link to the 
	  license, and indicate if changes were made. You may do so in any 
	  reasonable manner, but not in any way that suggests the licensor endorses
	  you or your use.

	> No additional restrictions — You may not apply legal terms or technological
	  measures that legally restrict others from doing anything the license permits.

See https://creativecommons.org/licenses/by/4.0/ for more details on the license.

However, the parameter files in the /CC0 directory are dedicated to the public domain via CC0.
See here for more details - https://creativecommons.org/publicdomain/zero/1.0/
